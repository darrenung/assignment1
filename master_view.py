import os
from Crypto.PublicKey import RSA

def decrypt_valuables(f):
    # TODO: For Part 2, you'll need to decrypt the contents of this file

    key = RSA.importKey(open('master_private.der', 'rb').read())
    enc_data = key.decrypt(f)
    print("file :" ,f)
    print("encrypted data: ",enc_data)
    return enc_data

###

if __name__ == "__main__":
    fn = input("Which file in pastebot.net does the botnet master want to view? ")
    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        os.exit(1)
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    decrypt_valuables(f)
