import os
import sys
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA


def sign_file(f):
    # TODO: For Part 2, you'll use public key crypto here   
    generate_RSA()
    key = RSA.importKey(open('master_private.der', 'rb').read())
    h = SHA.new()
    h.update(f)
    signer = PKCS1_v1_5.new(key)
    signature = signer.sign(h)
    #print(sys.getsizeof(signature))
    print("signature: ", signature)

    #print("original plaintext: ", f)
    #key2 = RSA.importKey(open('master_public.der', 'rb').read())
    #encrypt = key2.encrypt(f, '')
    #print("encrypted file: ", encrypt)
    #dec_data = key.decrypt(encrypt)
    #print ("decrypt: ", dec_data)   
    return signature + f
    
###

def generate_RSA(bits=2048):
    '''
    Generate an RSA keypair with an exponent of 65537 in PEM format
    param: bits The key length in bits
    Return private key and public key
    '''
    key = RSA.generate(bits, e=65537)
    print("generate_RSA")

    if os.stat('master_private.der').st_size > 0 and os.stat('master_public.der').st_size > 0:
        print("private and public key files are not empty")
    else:
        priv_file = open('master_private.der','wb')
        priv_file.write(key.exportKey('DER'))
        #print("private key: " , key.exportKey('DER'))
        priv_file.close()

        pub_file = open('master_public','wb')
        pub_file.write(key.publickey().exportKey('DER'))
        #print("public key: " , key.publickey().exportKey('DER'))
        pub_file.close()

###

if __name__ == "__main__":
    fn = input("Which file in pastebot.net should be signed? ")
    if not os.path.exists(os.path.join("pastebot.net", fn)):
        print("The given file doesn't exist on pastebot.net")
        os.exit(1)
    f = open(os.path.join("pastebot.net", fn), "rb").read()
    signed_f = sign_file(f)
    signed_fn = os.path.join("pastebot.net", fn + ".signed")
    out = open(signed_fn, "wb")
    out.write(signed_f)
    out.close()
    print("Signed file written to", signed_fn)


